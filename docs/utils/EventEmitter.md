#### EventEmitter

> 监听事件

```javascript
import { EventEmitter } from "ascor";

let event = new EventEmitter();

event.on("start", (arg) => {
	console.log(arg);
	// 123
});
//只执行1次
event.once("start", (arg) => {
	console.log(arg);
	// 123
});
event.emit("start", "123");
// 取消监听
event.off("start");
```
