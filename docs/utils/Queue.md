#### Queue

> 执行队列

```javascript
import { Queue } from "ascor";

let q = new Queue({
	concurrency: 10,
	autoStart: true,
});

let tasks = [...new Array(10)].map((m, i) => {
	return {
		index: i,
		retry: 3,
		job() {
			return new Promise((resolve, reject) => {
				setTimeout(() => {
					if (i % 2 == 1) {
						resolve(i);
					} else {
						reject(i);
					}
				}, 2000);
			});
		},
	};
});

// q.on("start", () => {
// 	console.log("start");
// });
// q.on("end", () => {
// 	console.log("end");
// });

q.on("success", (d) => {});
q.on("error", (d) => {});

q.push(...tasks.map((m) => m.job));
```
