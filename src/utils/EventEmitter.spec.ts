import { expect } from "chai";
import { EventEmitter } from "./EventEmitter";

describe("EventEmitter 类测试", () => {
	it("new EventEmitter().on('start',()=>{})", function () {
		let event = new EventEmitter();
		event.on("start", (arg) => {
			expect(arg).to.be.equal("startArg");
		});
		event.emit("start", "startArg");
	});
	it("new EventEmitter().once('start',()=>{})", function () {
		let event = new EventEmitter();
		let flag = "";
		event.once("start", (arg) => {
			flag = arg;
			expect(arg).to.be.equal("startArg");
		});
		event.emit("start", "startArg");
		event.emit("start", "startArg2");
		setTimeout(() => {
			expect(flag).to.be.equal("startArg");
		}, 300);
	});
	it("new EventEmitter().off('start')", function () {
		let event = new EventEmitter();
		let flag = "";
		event.on("start", (arg) => {
			flag = arg;
			expect(arg).to.be.equal("startArg");
		});
		event.emit("start", "startArg");
		event.off("start");
		event.emit("start", "startArg2");
		setTimeout(() => {
			expect(flag).to.be.equal("startArg");
		}, 300);
	});
	it("new EventEmitter().off('start',cb)", function () {
		let event = new EventEmitter();
		let flag = "";
		let cb = (arg) => {
			flag = arg;
			expect(arg).to.be.equal("startArg");
		};
		event.on("start", cb);
		event.on("start", (arg) => {
			flag = arg;
			expect(arg).to.be.ok;
		});
		event.emit("start", "startArg");
		event.off("start", cb);
		event.emit("start", "startArg2");
		setTimeout(() => {
			expect(flag).to.be.equal("startArg2");
		}, 300);
	});
});
