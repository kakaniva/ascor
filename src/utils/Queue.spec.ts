import { expect } from "chai";
import { Queue } from "./Queue";

describe("Queue 类测试", () => {
	it("new Queue()", function () {
		let q = new Queue({
			concurrency: 10,
			autoStart: true,
		});

		let tasks = [...new Array(100)].map((m, i) => {
			return {
				index: i,
				retry: 3,
				job() {
					return new Promise((resolve, reject) => {
						setTimeout(() => {
							if (i % 2 == 1) {
								resolve(i);
							} else {
								reject(i);
							}
						}, 2000);
					});
				},
			};
		});

		// q.on("start", () => {
		// 	console.log("start");
		// });
		// q.on("end", (err) => {
		// 	console.log("end", err);
		// });

		q.on("success", (d) => {
			expect(d % 2).to.be.equal(1);
		});
		q.on("error", (d) => {
			expect(d % 2).to.be.equal(2);
		});

		q.push(...tasks.map((m) => m.job));
	});
});
